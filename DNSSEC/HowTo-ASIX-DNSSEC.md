
![ej](images/Howtodnssec.png "ej")

## Index

1. [Parte Teórica](#teoria)

    + [DNS y DNSSEC](#DNS) 
    + [Funcionamiento DNSSEC](#dnssec)
  
2. [Parte Práctica](#practica)
	
	+ [Esquema de la práctica](#esquema)
	+ [CONFIGURACIÓN DNS](#config0)
    	+ [DNS PRINCIPAL CONFIGURACIÓN](#config1)
    	+ [DNS RECURSIVE CONFIGURACIÓN](#config2)
	+ [CONFIGURACIÓN DNSSEC](#config3)
    	+ [DNS SEGURA IMPLEMENTACIÓN EN AMBOS DNS](#implement)



## Parte Teórica <a name="teoria"></a> 

### DNS y DNSSEC <a name="DNS"></a> 

* El sistema DNS fué diseñado hace muchos años, cuando el internet aún no era un lugar tan poblado ni tan peligroso como actualmente.  
Este protocolo ya dispone de una pequeña protección contra intrusos maliciosos, pero no es suficiente a día de hoy y es altamente vulnerable a ataques como el spoofing.
Por esto mismo, se creo el DNSSEC (Domain Name System Security Extensions) unas extensiones de seguridad que añaden las firmas digitales en los datos del DNS para así poder validar la integridad y autenticidad de esta información.

* Esquema de un ejemplo de DNSSEC extraído de una pagina web:
![DNSsec](images/dnssec.jpg "dns")

* Documentación extraída de bind9 DNSSEC doc:

>DNS Security Extensions (DNSSEC) addresses this need, by adding digital signatures into DNS data so that each DNS response can be verified for integrity (the answer did not change during transit) and authenticity (the data came from the true source, not an impostor). In the ideal world, when DNSSEC is fully deployed, every single DNS answer can be validated and trusted.
>
>DNSSEC does not provide a secure tunnel; it does not encrypt or hide DNS data. It operates independently of an existing Public Key Infrastructure (PKI). It does not need SSL certificates or shared secrets. It was designed with backwards compatibility in mind, and can be deployed without impacting “old” unsecured domain names.
>


* El sistema DNSSEC se puede implementar los tres componentes principales de la estructura de DNS:

>Recursive Servers: People use recursive servers to lookup external domain names such as www.example.com. Operators of recursive servers need to enable DNSSEC validation. With validation enabled, recursive servers carry out additional tasks on each DNS response they receive to ensure its authenticity.  
>Authoritative Servers: People who publish DNS data on their name servers need to sign that data. This entails creating additional resource records, and publishing them to parent domains where necessary. With DNSSEC enabled, authoritative servers respond to queries with additional DNS data, such as digital signatures and keys, in addition to the standard answers.  
>Applications: This component lives on every client machine, from web servers to smart phones. This includes resolver libraries on different operating systems, and applications such as web browsers.  
>

### Funcionamiento DNSSEC  <a name="dnssec"></a>

* DNSSEC añade ocho resources records:

>RRSIG: With DNSSEC enabled, just about every DNS answer (A, PTR, MX, SOA, DNSKEY, etc.) comes with at least one resource record signature, or RRSIG. These signatures are used by recursive name servers, also known as validating resolvers, to verify the answers received. 
>
>DNSKEY: DNSSEC relies on public-key cryptography for data authenticity and integrity. There are several keys used in DNSSEC, some private, some public. The public keys are published to the world as part of the zone data, and they are stored in the DNSKEY record type.
>
>In general, keys in DNSSEC are used for one or both of the following roles: as a Zone Signing Key (ZSK), used to protect all zone data; or as a Key Signing Key (KSK), used to protect the zone’s keys. A key that is used for both roles is referred to as a Combined Signing Key (CSK). 
>
>DS: One of the critical components of DNSSEC is that the parent zone can “vouch” for its child zone. The DS record is verifiable information (generated from one of the child’s public keys) that a parent zone publishes about its child as part of the chain of trust. 
>
>NSEC, NSEC3, NSEC3PARAM: These resource records all deal with a very interesting problem: proving that something does not exist. 
>
>CDS, CDNSKEY: The CDS and CDNSKEY resource records apply to operational matters and are a way to signal to the parent zone that the DS records it holds for the child zone should be updated.
>


* La diferencia principal entre DNS y DNSSEC es que un servidor DNS solo pide la información del record mientras que DNSSEC pregunta también por la verificación de este.

>Traditional (insecure) DNS lookup is simple: a recursive name server receives a query from a client to lookup a name like www.isc.org. The recursive name server tracks down the authoritative name server(s) responsible, sends the query to one of the authoritative name servers, and waits for it to respond with the answer.
>
>With DNSSEC validation enabled, a validating recursive name server (a.k.a. a validating resolver) asks for additional resource records in its query, hoping the remote authoritative name servers respond with more than just the answer to the query, but some proof to go along with the answer as well. If DNSSEC responses are received, the validating resolver performs cryptographic computation to verify the authenticity (the origin of the data) and integrity (that the data was not altered during transit) of the answers, and even asks the parent zone as part of the verification. It repeats this process of get-key, validate, ask-parent, and its parent, and its parent, all the way until the validating resolver reaches a key that it trusts. In the ideal, fully deployed world of DNSSEC, all validating resolvers only need to trust one key: the root key.
>


## Parte Práctica <a name="practica"></a>

## Esquema de la práctica   <a name="esquema"></a>
*	![DNSSEC](images/esquema.png "esquema")

## CONFIGURACIÓN DNS <a name="config0"></a> 

### DNS PRINCIPAL CONFIGURACIÓN  <a name=" config1"></a>
* Instalación de **bind9** en el DNS principal:
	```
	root@i05:~# apt-get install bind9
	```
* Configuración de **/etc/bind/named.conf.default-zones** DNS principal:
	```
	zone "edt" {
		type master;
		file "/etc/bind/db.edt";
	};
	```
* Configuración de **/etc/bind/db.edt** DNS principal:
	```
	@ SOA ns root 1 4 4 4 4
   	  NS ns
	ns A 10.200.243.217
	
	sabadell	NS	ns.sabadell
	ns.sabadell	A	10.200.243.218
	```
* Configuración de **/etc/bind/named.conf.options**

### DNS RECURSIVE CONFIGURACIÓN <a name="config2"></a> 
* Instalación de **bind9** en el DNS recursive.

* Configuración de **/etc/bind/named.conf.default-zones** DNS recursive:
	```
	zone "edt"{
		type forward;
		forwarders{
		10.200.243.217;};
	};
	
	```
* Configuración de **/etc/bind/db.edt** DNS recursive:
	```
	@ SOA ns admin 1 4 4 4 4
          NS      ns
	bueno.sabadell.edt. A	10.200.243.206
	www  CNAME bueno.sabadell.edt.
	```	
* Configuración de **/etc/bind/named.conf.options** DNS recursive:
	```
	options {
		directory "/var/cache/bind";
		forwarders {
	 		10.200.243.217;
	 	};
		dnssec-validation no;
		listen-on-v6 { any; };
	};
	```
	
### CONFIGURACIÓN DEL DNSSEC <a name="config3"></a> 

### DNS SEGURA IMPLEMENTACIÓN EN AMBOS DNS <a name="implement"></a> 

* Se ha cambiado las rutas de el fichero /etc/bind/named.conf debido a que el demonio bind no tiene forma de crear 2 archivos que necesita por que el propio sistema no se lo deja crear en el directorio /etc/bind:
	```
	include "/var/cache/bind/zones/named.conf.options";
	include "/var/cache/bind/zones/named.conf.local";
	include "/var/cache/bind/zones/named.conf.default-zones";
	```
* Conceder permisos a bind en sus directorios:
	```
	chown bind /var/cache/bind/
	chown bind /var/cache/bind/*
	```
* Creación de las llaves en /var/cache/bind/zones/keys:
	```
	dnssec-keygen -a RSASHA256 -b 1280 sabadell.edt

	dnssec-keygen -a RSASHA256 -b 2048 -f KSK sabadell.edt
	```
* Cambiar la zona para que pase a ser segura en /var/cache/bind/zones/named.conf.:
	```
	zone "edt" {
        type master;
        file "/var/cache/bind/zones/db.edt.signed";
        inline-signing yes;
        auto-dnssec maintain;
        serial-update-method increment;
	};
	```
* Cambiar config de /var/cache/bind/zones/named.conf.options:
	```
	options {
        	directory "/var/cache/bind/zones";
		dnssec-enable yes;
        	dnssec-validation yes;
        	key-directory "/var/cache/bind/zones/keys";
        	listen-on-v6 { any; };
	};
	```
* Firmar el nuevo certificado:
	```
	dnssec-signzone -S -K /var/cache/bind/zones/keys -g -a -o edt db.edt
	```
* Comprobar que todo funciona correctamente:
	```
	dig ns.edt +dnssec

	; <<>> DiG 9.16.27-Debian <<>> ns.edt +dnssec
	;; global options: +cmd
	;; Got answer:
	;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27301
	;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

	;; OPT PSEUDOSECTION:
	; EDNS: version: 0, flags: do; udp: 1232
	; COOKIE: cdf292d7a273034f010000006274f043f56f1772ba0416c1 (good)
	;; QUESTION SECTION:
	;ns.edt.                                IN      A

	;; ANSWER SECTION:
	ns.edt.                 4       IN      A       10.200.243.217
	ns.edt.                 4       IN      RRSIG   A 8 2 4 20220516104507 20220506073822 25063 edt. ocYd3k16A0IYJTYRwJWBy/cIlkED9HrqLUDf166jM2gynj3jvY29NE7B qRyifeguIkiIvJ/UTDOfVw2OA0A7/R9lH45zqKwmrLE0qWUWCvU+rmzR YRJEMWmiPxfEI4miF	    4yX4BHtaJJJG1n1i+Il2Iw2vsGDVCGx8z6pQ7RL x4gN9FBst8XjhogrzKSdAALUaDWj1ISgg5eauhe1cN94ag==

	;; Query time: 0 msec
	;; SERVER: 10.200.243.217#53(10.200.243.217)
	;; WHEN: Fri May 06 11:54:11 CEST 2022
	;; MSG SIZE  rcvd: 274
	```
