
![x](images/spoofing.png "x")

## Spoofing
# Indice de contenido
1. [Teoria Spoofing](#spoofing)
	+ [Tipos de spoofing](#ataques)
		* [Spoofing and TCP/IP](#TCP)
		* [Domain name](#domain)
		* [Referrer spoofing](#referrer)
		* [Poisoning](#poisoning)
		* [e-mail addres spoffing](#e-mail)
		* [geolocation](#geolocation)
2. [Practica Spoofing](#practica)
	+ [Demostración spoffing](#demo)
		* [Esquema hosts](#esquema)
		* [Trafico de red normal](#normal)
		* [Trafico con ataque](#ataque)
		* [Demostracion de ataque](#demostracion)

## Teoria: <a name="spoofing"></a>
El spoofing es un método de ataque basado en la falsificación (de ahí proviene su nombre en inglés) para engañar a la victima y obtener algo. Su objetivo es desde  obtener tu ip, hasta tarjetas de crédito, credenciales de banco o cualquier tipo de información sensible. El mecanismo de obtención de datos es simple, el atacante hace una pagina web falsa de algún lugar que tengas que iniciar session con tus credenciales. El atacante envía a la victima a traves de un medio de información el link, la victima entra a traves del link y inicia session. En ese momento el atacante ya tiene las credenciales de la victima. Hay diversos tipos de spoofing.

* Diferentes tipos de Spoofing (extraído de [Wikipedia](https://en.wikipedia.org/wiki/Spoofing_attack)): <a name="ataques"></a>

>**Spoofing and TCP/IP:** <a name="TCP"></a>
Many of the protocols in the TCP/IP suite do not provide mechanisms for authenticating the source or destination of a message, leaving them vulnerable to spoofing attacks when extra precautions are not taken by applications to verify the identity of the sending or receiving host. IP spoofing and ARP spoofing in particular may be used to leverage man-in-the-middle attacks against hosts on a computer network. Spoofing attacks which take advantage of TCP/IP suite protocols may be mitigated with the use of firewalls capable of deep packet inspection or by taking measures to verify the identity of the sender or recipient of a message.
>
>**Domain name spoofing:** <a name="domain"></a>
The term 'Domain name spoofing' (or simply though less accurately, 'Domain spoofing') is used generically to describe one or more of a class of phishing attacks that depend on falsifying or misrepresenting an internet domain name.These are designed to persuade unsuspecting users into visiting a web site other than that intended, or opening an email that is not in reality from the address shown (or apparently shown). Although website and email spoofing attacks are more widely known, any service that relies on domain name resolution may be compromised.
>
>**Referrer spoofing:** <a name="referrer"></a>
Some websites, especially pornographic paysites, allow access to their materials only from certain approved (login-) pages. This is enforced by checking the referrer header of the HTTP request. This referrer header, however, can be changed (known as "referrer spoofing" or "Ref-tar spoofing"), allowing users to gain unauthorized access to the materials.
>
>**Poisoning of file-sharing networks:** <a name="poisoning"></a>
"Spoofing" can also refer to copyright holders placing distorted or unlistenable versions of works on file-sharing networks.
>
>**E-mail address spoofing:** <a name="e-mail"></a>
The sender information shown in e-mails (the From: field) can be spoofed easily. This technique is commonly used by spammers to hide the origin of their e-mails and leads to problems such as misdirected bounces (i.e. e-mail spam backscatter).
>
>E-mail address spoofing is done in quite the same way as writing a forged return address using snail mail. As long as the letter fits the protocol, (i.e. stamp, postal code) the Simple Mail Transfer Protocol (SMTP) will send the message. It can be done using a mail server with telnet.[5]
>
>**Geolocation:** <a name="geolocation"></a>
Geolocation spoofing occurs when a user applies technologies to make their device appear to be located somewhere other than where it is are actually located.[6] The most common geolocation spoofing is through the use of a Virtual Private Network (VPN) or DNS Proxy in order for the user to appear to be located in a different country, state or territory other than where they are actually located. According to a study by GlobalWebIndex, 49% of global VPN users utilize VPNs primarily to access territorially restricted entertainment content.[7] This type of geolocation spoofing is also referred to as geo-piracy, since the user is illicitly accessing copyrighted materials via geolocation spoofing technology. Another example of geolocation spoofing occurred when an online poker player in California used geolocation spoofing techniques to play online poker in New Jersey, in contravention of both California and New Jersey state law.[8] Forensic geolocation evidence proved the geolocation spoofing and the player forfeited more than $90,000 in winnings.
>
## Practica: <a name="practica"></a>
### Demostración spoffing: <a name="demo"></a>
- Tres host <a name="esquema"></a>
	- Kali(atacante): 10.200.243.206
	- Servidor web: 10.200.243.204
	- DNS: 10.200.243.209
	- Cliente: 10.200.243.205

- Miramos con wireshark el trafico de red normal: <a name="normal"></a>
	

	![wire](images/wireshark-normal.png "wire")

- Comenzamos el ataque desde el kali y miramos el trafico ahora con wireshark: <a name="ataque"></a>
	
	![wire2](images/wireshark-atack.png "wire2")
 	
- Demostracion de que es la pagina web del atacante desde el cliente al entrar en www.sabadell.edt: <a name="demostracion"></a>
	
	![spoffing](images/captura-spoofing.png "spoffing")




