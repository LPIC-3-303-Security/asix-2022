![x](images/phishing.png "x")

# Indice de contenido

1. [Phishing](#phishing)

    + [Teoría](#teoria) 
    + [Tipos de ataques ](#ataques)
        * [Email phishing](#mail)
        * [Spear phishing](#spear)
        * [Clone phishing](#clone)
        * [Voice phishing](#voice)
        * [SMS phishing](#sms)
        * [Page hijacking](#hijack)
    + [Medidas de protección](#protect)

2. [Ejemplos Practicos](#practico)
    * [Practica mail phishing](#mphishing)
      + [Explicacion del ataque](#explicacion1)
      + [Herramienta utilizada](#herramienta1)
      + [Descarga de la herramienta](#descarga1)
      + [Ejecución del ataque ](#ataque1)
  
    * [Practica sms phishing](#sphidhing)
      + [Explicacion del ataque](#explicacion2)
      + [Herramienta utilizada](#herramienta2)
      + [Descarga de la herramienta](#descarga2)
      + [Ejecución del ataque ](#ataque2)

## Phishing <a name="phishing"></a> 

## Teoría <a name="teoria"></a> 

El phishing es unos de los ataques más comunes en el día de hoy, todo esto gracias a que todo el mundo dispone de correo electrónico, teléfono o formas de comunicarse con el resto. Usualmente no es un ataque que sea muy efectivo en gente con conocimientos básicos de informática, por eso su target son personas mayores o gente que no tenga mucho conocimiento sobre este sector.

En resumen, el phishing no es más que una explotación de la ingeniería social mediante la cual el atacante se hace pasar por un servicio o tercero haciendo uso de la confianza/ignorancia de algunas victimas para obtener datos sensibles de esta. Es uno de los ataques más sencillos y efectivos, esto gracias al desconocimiento e imprudencia de los usuarios casuales, quienes no tienen mucho conocimiento de informática o ciberseguridad, especialmente la gente mayor.

## Tipos de ataque <a name="ataques"></a> 

El tipo de phishing más común es el email phishing que se basa en enviar un link malicioso en un correo con la intención de robar credenciales a la víctima.
A continuación se muestran los diferentes tipos de phishing.

* Diferentes tipos de phishing, extraído de [Wikipedia](https://en.wikipedia.org/wiki/Phishing):

>
>Email phishing <a name="mail"></a> 
>Most phishing messages are delivered by email, and are not personalized or targeted to a specific individual or company–this is termed "bulk" phishing. The content of a bulk phishing message varies widely depending on the goal of the attacker–common targets for impersonation include banks and financial services, email and cloud productivity providers, and streaming services. Attackers may use the credentials obtained to directly steal money from a victim, although compromised accounts are often used instead as a jumping-off point to perform other attacks, such as the theft of proprietary information, the installation of malware, or the spear phishing of other people within the target's organization. Compromised streaming service accounts are usually sold directly to consumers on darknet markets.
>
>Spear phishing <a name="spear"></a> 
>Spear phishing involves an attacker directly targeting a specific organization or person with tailored phishing communications. This is essentially the creation and sending of emails to a particular person to make the person think the email is legitimate. In contrast to bulk phishing, spear phishing attackers often gather and use personal information about their target to increase their probability of success of the attack. Spear phishing typically targets executives or those that work in financial departments that have access to the organization's sensitive financial data and services. A 2019 study showed that accountancy and audit firms are frequent targets for spear phishing owing to their employees' access to information that could be valuable to criminals.
>
>Threat Group-4127 (Fancy Bear) used spear phishing tactics to target email accounts linked to Hillary Clinton's 2016 presidential campaign. They attacked more than 1,800 Google accounts and implemented the accounts-google.com domain to threaten targeted users.
>
>A recent study tested the susceptibility of certain age groups against spear fishing. In total, 100 young and 58 older users received, without their knowledge, daily simulated phishing emails over 21 days. A browser plugin recorded their clicking on links in the emails as an indicator of their susceptibility. Forty-three percent of users fell for the simulated phishing emails, with older women showing the highest susceptibility. While susceptibility in young users declined across the study, susceptibility in older users remained stable.
>
>
>Clone phishing <a name="clone"></a> 
>Clone phishing is a type of phishing attack whereby a legitimate, and previously delivered email containing an attachment or link has had its content and recipient address(es) taken and used to create an almost identical or cloned email. The attachment or link within the email is replaced with a malicious version and then sent from an email address spoofed to appear to come from the original sender. It may claim to be a resend of the original or an updated version to the original. Typically this requires either the sender or recipient to have been previously hacked for the malicious third party to obtain the legitimate email.
>
>Voice phishing <a name="voice"></a> 
>Main article: Voice phishing
>Voice phishing, or vishing, is the use of telephony (often Voice over IP telephony) to conduct phishing attacks. Attackers will dial a large quantity of telephone numbers and play automated recordings - often made using text-to-speech synthesizers - that make false claims of fraudulent activity on the victim's bank accounts or credit cards. The calling phone number will be spoofed to show the real number of the bank or institution impersonated. The victim is then directed to call a number controlled by the attackers, which will either automatically prompt them to enter sensitive information in order to "resolve" the supposed fraud, or connect them to a live person who will attempt to use social engineering to obtain information.[28] Voice phishing capitalizes on the lower awareness among the general public of techniques such as caller ID spoofing and automated dialing, compared to the equivalents for email phishing, and thereby the inherent trust that many people have in voice telephony.
>
>SMS phishing <a name="sms"></a> 
>
>SMS phishing or smishing is conceptually similar to email phishing, except attackers use cell phone text messages to deliver the "bait". Smishing attacks typically invite the user to click a link, call a phone number, or contact an email address provided by the attacker via SMS message. The victim is then invited to provide their private data; often, credentials to other websites or services. Furthermore, due to the nature of mobile browsers, URLs may not be fully displayed; this may make it more difficult to identify an illegitimate logon page. As the mobile phone market is now saturated with smartphones which all have fast internet connectivity, a malicious link sent via SMS can yield the same result as it would if sent via email. Smishing messages may come from telephone numbers that are in a strange or unexpected format.
>
>Page hijacking <a name="hijack"></a> 
>Page hijacking involves compromising legitimate web pages in order to redirect users to a malicious website or an exploit kit via cross site scripting. A hacker may compromise a website and insert an exploit kit such as MPack in order to compromise legitimate users who visit the now compromised web server. One of the simplest forms of page hijacking involves altering a webpage to contain a malicious inline frame which can allow an exploit kit to load. Page hijacking is frequently used in tandem with a watering hole attack on corporate entities in order to compromise targets.
>

Como podemos observar, hay una amplia variedad de ataques phishing a día de hoy, pero los más comunes y en los que nos centraremos hoy son el phishing por email y por sms o smshing. Existen muchas herramientas para ejecutar estos ataques, en esta práctica usaremos zphisher (https://github.com/htr-tech/zphisher) para obtener un clon de un servicio que queramos suplantar y fake-sms (https://github.com/machine1337/fake-sms) una herramienta para enviar sms falsos a nuestra victima.

## Medidas de protección <a name="protect"></a> 

Este tipo de ataques son relativamente fáciles de evitar, basta con filtrar el correo y no pulsar en ningún link sospechoso en el caso del email phishing y sus variantes. Por otro lado, en el caso de los ataques por teléfono hay que verificar que los sms se envían de un número verificado y en caso de recibir llamadas con presuntos cargos en nuestras cuentas, lo primero que habría que hacer es comunicarse con nuestras entidades y verificar con ellos los presuntos cargos.


# Ejemplos Practicos]<a name="practico"></a> 

#### Email phishing <a name="mphishing"></a> 

## Explicación del ataque <a name="explicacion1"></a> 

Comenzaremos con el ataque más utilizado, el phishing por email. En este caso práctico encenderemos un servidor web con zphisher que imita el login de Instagram, una aplicación que actualmente gran parte de la población usa como red social. Una vez el servidor este up, enviaremos un correo a la victima imitando la plantilla que usa el propio Instagram para informar de un intento de inicio de sesión sospechoso. La plantilla la obtendremos con una extensión del propio chrome que nos permite crear templates a partir de correos que hayamos recibido. Una vez enviado, la victima será redirigida a la página falsa y esta captara la actividad de la victima y guardará los datos que esta ponga en la web. Una vez la victima intente acceder, la página se recargará y sera redirigida a la original.

## Herramienta utilizada <a name="herramienta1"></a> 

En esta práctica usaremos la herramienta zphisher del usuario htr-tech en github. Esta herramienta nos permitirá crear templates de los logins de diferentes páginas web y redes sociales que a su vez hosteará en nuestro equipo. También se encargará tanto de guardar toda la información que la victima introduzca en la página como de darnos la dirección ip de esta misma.

## Descarga de la herramienta  <a name="descarga1"></a> 

* Descarga de la herramienta y utilización

```sh

git clone https://github.com/htr-tech/zphisher

```

## Ejecución del ataque <a name="ataque1"></a>


```sh
cd zphisher
bash zphisher.sh
```

Una vez ejecutamos el script se desplegara el siguiente menú

![ej](images/zphisher.png "ej")

Este menú nos indica los posibles servicios que podemos suplantar. En nuestro caso usaremos el número 2, que corresponde a instagram.

A continuación nos saldrá este menú con las opciones para desplegar el servidor web, en nuestro caso lo haremos con la opción 3 Cloudflare.

![ej](images/menu1zphishing.png "ej")

Aquí se nos dará un link el cual usaremos para engañar a nuestra victima y redirigida a la página clonada.

Este link lo introduciremos en un template de instagram y se lo enviaremos a la victima, quien entrará pensando que es un correo legítimo y que, asustada por el aviso, introducirá sus datos en la página.

Este es el correo que enviaremos a la Victima

![ej](images/mailfake.png "ej")

Aquí vemos como el mensaje es básicamente una copia del template que instagram envía en caso de recibir un intento de inicio de sesión sospechoso. También podemos ver que el link estará oculto en los botones que nos dan las opciones de iniciar sesión o cambiar de contraseña, por lo que la victima en ningún momento antes de pulsar en el link será capaz de ver que se trata de un link diferente al de instagram que la redirigirá a una página falsa.


Y esto es lo que la victima recibirá en su cuenta de correo, observemos como al ser un template se indica que la dirección de correo es la original de instagram y no se ven los links como hemos mencionado antes. (Nota: enviamos el correo desde una cuenta que se vea claramente que es una prueba por seguridad, aunque podríamos alterarlo y hacerlo más creíble todavía.)

![ej](images/phishingrecibido.png "ej")


Una vez la victima clique sobre cualquiera de los dos links, será dirigida a la siguiente página. Podemos ver como la url delata que no se trata del instagram original, pero esto es algo que mucha gente ignoraría por falta de atención o de conocimientos.

![ej](images/intagramfake.png "ej")


Por último, en la siguiente imagen podemos observar como los datos que introduzca la victima serás mostrados por pantalla y a su vez guardado en un archivo.

![ej](images/menufinalzphisher.png "ej")



#### Smshing <a name="sphishing"></a> 

## Explicación del ataque <a name="explicacion2"></a> 

Este ataque se base en enviar un sms a nuestra victima para lograr obtener datos de la misma. En esta prueba simularemos un ataque de phishing por sms o smshing con al herramienta fake-sms, que nos proporcionará la capacidad de enviar sms a cualquier número, pero sin links para evitar su uso malicioso. En esta demostración por un tema de seguridad, usaremos un número de teléfono online que solo puede recibir sms.

## Herramienta utilizada <a name="herramienta2"></a> 

En esta práctica usaremos la herramienta fake-sms del usuario htr-tech en github. Esta herramienta nos permitirá crear sms falsos con un número aleatorio generado por la herramienta.

## Descarga de la herramienta  <a name="descarga2"></a> 

```sh
git clone https://github.com/htr-tech/fake-sms

```

## Ejecución del ataque <a name="ataque2"></a>

```sh
cd fake-sms

bash fakesms.sh
```

Una vez ejecutamos el script se desplegara el siguiente menú

![ej](images/smshingmenu.png "ej")

Aquí seleccionaremos nuestro ataque e introduciremos el número al cual queremos atacar, en nuestro caso usaremos un número online que solo recibe sms. 


* Escribimos el mensaje que queremos enviar y confirmamos que llega al numero en cuestión.

![ej](images/demostracion-smshing.png "ej")
